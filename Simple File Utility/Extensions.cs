﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public static class Extensions
{

    public static bool Delete(this FileInfo file, bool no_use)
    {
        try
        {
            file.Delete();
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
    }

    public static bool Delete(this DirectoryInfo directory, bool no_use)
    {
        try
        {
            directory.Delete();
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
    }

    public static bool OpenForm(this Form this_Form, string form_name)
    {
        foreach (Form frm in Application.OpenForms)
        {
            if (!frm.Text.Equals(form_name))
                continue;

            frm.Show();
            this_Form.Hide();
            return true;
        }

        return false;
    }
}
