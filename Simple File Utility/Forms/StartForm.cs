﻿using System;
using System.Windows.Forms;

namespace SimpleFileFinder
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this.OpenForm("Duplicate_File_Form"))
            {
                var startForm = new DuplicateFlleFinder.DuplicateFileForm();
                startForm.Show();
                Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!this.OpenForm("Empty_Folder_Form"))
            {
                var startForm = new EmptyFolderFinder.Empty_Folder_Form();
                startForm.Show();
                Hide();
            }
        }
    }
}
