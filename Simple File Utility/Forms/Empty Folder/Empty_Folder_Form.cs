﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmptyFolderFinder
{
    public partial class Empty_Folder_Form : Form
    {
        public Empty_Folder_Form()
        {
            InitializeComponent();

            this.AllowDrop = true;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (!this.OpenForm("StartForm"))
            {
                var startForm = new SimpleFileFinder.StartForm();
                Hide();
                startForm.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form.ReflectMessage(, Message.Create.)
            //Application.Restart();
            //Application.UseWaitCursor = true;

            listBox1.Items.Clear();
            foreach (var root in new DirectoryInfo(@"E:\Testing").GetDirectories())
            {
                var WorkingDir = root;
                var ScaningDir = root;

                int index = 0;
                do
                {
                    if(index > ScaningDir.GetDirectories().Length)
                    {
                        index = 0;
                        
                    }
                    else
                    {
                        
                    }
                } 
                while (WorkingDir.GetDirectories().Length > 0);

                listBox1.Items.Add(root.Name);
            }
        }

        private List<FileSystemInfo> GetDirectorys(DirectoryInfo dir)
        {
            return dir.GetFileSystemInfos("*", SearchOption.TopDirectoryOnly).Where(x => x.Attributes.ToString() == "Directory").ToList();
        }

        private void Empty_Folder_Form_DragEnter(object sender, DragEventArgs e)
        {
            Console.WriteLine("Drag entered");

            foreach (var item in (string [])e.Data.GetData(DataFormats.FileDrop))
            {
                Console.WriteLine(item);
            }
        }
    }
}
