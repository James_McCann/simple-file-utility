﻿using System;
using System.IO;
using System.Windows.Forms;
using Duplicate_File_Finder.FileHandlers;

namespace Duplicate_File_Finder.Forms
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            Playsoundcheckbox.Checked = Properties.Settings.Default.PlaySound;
            PlayTimeSlidBar.Value = Properties.Settings.Default.PlayTime;
            CustomSoundFileCheckBox.Checked = Properties.Settings.Default.CustomSoundFile;
            label1.Text = $@"PlayTime: = {TimeSpan.FromMilliseconds(PlayTimeSlidBar.Value).TotalSeconds} Seconds";
            CloseButton.BringToFront();

            listBox1.DataSource = FileFunc.IgnoredFiletypes;//TODO: fix

            if (!File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav"))
            {
                CustomSoundFileCheckBox.Checked = false;
                Properties.Settings.Default.CustomSoundFile = false;
                Properties.Settings.Default.Save();
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Text.Equals("Duplicate_File_Form"))
                {
                    frm.Show();
                    Hide();
                    return;
                }
            }
        }

        private void Playsoundcheckbox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PlaySound = Playsoundcheckbox.Checked;
            Properties.Settings.Default.Save();
        }

        private void PlayTimeSlidBar_ValueChanged(object sender, EventArgs e)
        {
            if(PlayTimeSlidBar.Value > 500)
            {
                label1.Text = $@"PlayTime: = {TimeSpan.FromMilliseconds(PlayTimeSlidBar.Value).TotalSeconds} Seconds";
            }
            else
            {
                label1.Text = @"Cant go below Half a Second";
                PlayTimeSlidBar.Value = 500;
            }
        }

        private void PlayTimeSlidBar_ValueChangeComplete(object sender, EventArgs e)
        {
            Properties.Settings.Default.PlayTime = PlayTimeSlidBar.Value;
            Properties.Settings.Default.Save();
        }

        public static bool Checkfile(string path)
        {
            try
            {
                File.Open(path, FileMode.Open, FileAccess.Read).Dispose();
                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        private void SetSoundFile()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                if (Checkfile(openFile.FileName))
                {
                    if (Path.GetExtension(openFile.FileName.ToLower()).Equals(".wav".ToLower()))
                    {
                        if (File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav"))
                        {
                            File.Delete($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav");
                        }
                        File.Copy(openFile.FileName, $@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile{Path.GetExtension(openFile.FileName)}");
                    }
                    else
                    {
                        {
                            MessageBox.Show(@"Supported Files Types (.wav )", @"File Not a Sound File");
                        }
                    }
                }
                else
                {
                    MessageBox.Show(@"Failed To Get File Stream");
                }
            }
        }

        private void CustomSoundFileCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(CustomSoundFileCheckBox.Checked)
            {
                if (!File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav"))
                {
                    SetSoundFile();
                }
            }
            Properties.Settings.Default.CustomSoundFile = CustomSoundFileCheckBox.Checked;
            Properties.Settings.Default.Save();
        }

        private void SetSoundFileLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SetSoundFile();
        }

        private void PlaySoundLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!FileFunc.Soundplayer.IsAlive)
                FileFunc.Soundplayer.Start();
        }
    }
}
