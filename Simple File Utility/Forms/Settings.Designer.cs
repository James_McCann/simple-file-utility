﻿namespace Duplicate_File_Finder.Forms
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.Playsoundcheckbox = new System.Windows.Forms.CheckBox();
            this.PlayTimeSlidBar = new Bunifu.Framework.UI.BunifuSlider();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PlaySoundLabel = new System.Windows.Forms.LinkLabel();
            this.SetSoundFileLabel = new System.Windows.Forms.LinkLabel();
            this.CustomSoundFileCheckBox = new System.Windows.Forms.CheckBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.CloseButton = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CloseButton)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // Playsoundcheckbox
            // 
            this.Playsoundcheckbox.AutoSize = true;
            this.Playsoundcheckbox.Location = new System.Drawing.Point(331, 15);
            this.Playsoundcheckbox.Name = "Playsoundcheckbox";
            this.Playsoundcheckbox.Size = new System.Drawing.Size(80, 17);
            this.Playsoundcheckbox.TabIndex = 13;
            this.Playsoundcheckbox.Text = "Play Sound";
            this.Playsoundcheckbox.UseVisualStyleBackColor = true;
            this.Playsoundcheckbox.CheckedChanged += new System.EventHandler(this.Playsoundcheckbox_CheckedChanged);
            // 
            // PlayTimeSlidBar
            // 
            this.PlayTimeSlidBar.BackColor = System.Drawing.Color.Transparent;
            this.PlayTimeSlidBar.BackgroudColor = System.Drawing.Color.DarkGray;
            this.PlayTimeSlidBar.BorderRadius = 0;
            this.PlayTimeSlidBar.IndicatorColor = System.Drawing.Color.SeaGreen;
            this.PlayTimeSlidBar.Location = new System.Drawing.Point(8, 31);
            this.PlayTimeSlidBar.MaximumValue = 5000;
            this.PlayTimeSlidBar.Name = "PlayTimeSlidBar";
            this.PlayTimeSlidBar.Size = new System.Drawing.Size(415, 30);
            this.PlayTimeSlidBar.TabIndex = 14;
            this.PlayTimeSlidBar.Value = 0;
            this.PlayTimeSlidBar.ValueChanged += new System.EventHandler(this.PlayTimeSlidBar_ValueChanged);
            this.PlayTimeSlidBar.ValueChangeComplete += new System.EventHandler(this.PlayTimeSlidBar_ValueChangeComplete);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Na";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(589, 319);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PlaySoundLabel);
            this.groupBox2.Controls.Add(this.SetSoundFileLabel);
            this.groupBox2.Controls.Add(this.PlayTimeSlidBar);
            this.groupBox2.Controls.Add(this.CustomSoundFileCheckBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.Playsoundcheckbox);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 100);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sound Settings";
            // 
            // PlaySoundLabel
            // 
            this.PlaySoundLabel.AutoSize = true;
            this.PlaySoundLabel.Location = new System.Drawing.Point(290, 64);
            this.PlaySoundLabel.Name = "PlaySoundLabel";
            this.PlaySoundLabel.Size = new System.Drawing.Size(61, 13);
            this.PlaySoundLabel.TabIndex = 18;
            this.PlaySoundLabel.TabStop = true;
            this.PlaySoundLabel.Text = "Play Sound";
            this.PlaySoundLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.PlaySoundLabel_LinkClicked);
            // 
            // SetSoundFileLabel
            // 
            this.SetSoundFileLabel.AutoSize = true;
            this.SetSoundFileLabel.Location = new System.Drawing.Point(357, 64);
            this.SetSoundFileLabel.Name = "SetSoundFileLabel";
            this.SetSoundFileLabel.Size = new System.Drawing.Size(56, 13);
            this.SetSoundFileLabel.TabIndex = 18;
            this.SetSoundFileLabel.TabStop = true;
            this.SetSoundFileLabel.Text = "Select File";
            this.SetSoundFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SetSoundFileLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SetSoundFileLabel_LinkClicked);
            // 
            // CustomSoundFileCheckBox
            // 
            this.CustomSoundFileCheckBox.AutoSize = true;
            this.CustomSoundFileCheckBox.Location = new System.Drawing.Point(209, 15);
            this.CustomSoundFileCheckBox.Name = "CustomSoundFileCheckBox";
            this.CustomSoundFileCheckBox.Size = new System.Drawing.Size(114, 17);
            this.CustomSoundFileCheckBox.TabIndex = 16;
            this.CustomSoundFileCheckBox.Text = "Custom Sound File";
            this.CustomSoundFileCheckBox.UseVisualStyleBackColor = true;
            this.CustomSoundFileCheckBox.CheckedChanged += new System.EventHandler(this.CustomSoundFileCheckBox_CheckedChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 125);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 186);
            this.listBox1.TabIndex = 18;
            // 
            // CloseButton
            // 
            this.CloseButton.Image = global::Duplicate_File_Finder.Properties.Resources.Close;
            this.CloseButton.Location = new System.Drawing.Point(584, 1);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(27, 23);
            this.CloseButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.CloseButton.TabIndex = 12;
            this.CloseButton.TabStop = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 343);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CloseButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Settings";
            this.Opacity = 0.99D;
            this.Text = "Setings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CloseButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.PictureBox CloseButton;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.CheckBox Playsoundcheckbox;
        private Bunifu.Framework.UI.BunifuSlider PlayTimeSlidBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox CustomSoundFileCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.LinkLabel SetSoundFileLabel;
        private System.Windows.Forms.LinkLabel PlaySoundLabel;
        private System.Windows.Forms.ListBox listBox1;
    }
}