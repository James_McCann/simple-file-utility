﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Duplicate_File_Finder.FileHandlers;

namespace Duplicate_File_Finder.Forms
{
    public partial class DuplicateFileForm : Form
    {
        private string _scanDir;

        private readonly FileHandler _fileHandler = new FileHandler();

        public DuplicateFileForm()//TODO: add hash db support(would chexk las moded time to see if hash changed)
        {
            InitializeComponent();
            _fileHandler.FileHandled += FileHandled;
            _fileHandler.FilesHandled += FilesHandled;

            FileFunc.IgnoredFiletypes.AddRange(new string[] { ".lock", ".ide-shm" });
            //Scanbuton.Enabled = false;
        }

        private void FilesHandled(object sender, AllFilesProssedEventArgs args)
        {
            FileFunc.StopScan = false;
            UpdatTick.Stop();

            if(!args.Failed)
            {
                Path_Box.Enabled = true;
                Fileinfo_panel.Enabled = true;
                Scanbuton.Text = @"Start Scan";
            }
            else
            {
                Path_Box.Enabled = true;
                Path_Box.Text = @"Folder access denied";
                Scanbuton.Text = @"Start Scan";
            }
            ScanprogressBar.Value = 100;
            ProgresssLable.Text = @"%100";

            if (SourceFiles_listBox.Items.Count == 0)
                MessageBox.Show("No Dupes Found..");
        }

        private void FileHandled(object sender, FileEventArgs e)
        {
            SourceFiles_listBox.Items.Add($"{e.Dup.SourceFile.Name}");
        }

        public static void InvokeCrossThread(Control control, Action action) => control.Invoke(action);

        private void Scan_button_Click(object sender, EventArgs e)
        {
            //_scanDir = @"D:\Users\Brandon\Desktop\ttt";

            if (Scanbuton.Text == @"Start Scan")
            {
                _fileHandler.InitializeScan(this, _scanDir);
                SourceFiles_listBox.Items.Clear();
                Fileinfo_panel.Enabled = false;
                Path_Box.Enabled = false;
                Scanbuton.Text = @"Stop Scan";
                UpdatTick.Start();
            }
            else
            {
                FileFunc.StopScan = true;
            }
        }
            //}
            //else
            //{
            //    FileFunc.StopScan = true;
            //    Scanbuton.Text = @"Stopping";
            //}
        }
            //}
            //else
            //{
            //    FileFunc.StopScan = true;
            //    Scanbuton.Text = @"Stopping";
            //}
        }
            //}
            //else
            //{
            //    FileFunc.StopScan = true;
            //    Scanbuton.Text = @"Stopping";
            //}
        }
            //}
            //else
            //{
            //    FileFunc.StopScan = true;
            //    Scanbuton.Text = @"Stopping";
            //}
        }

        private bool SetupDirBool()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _scanDir = folderBrowserDialog.SelectedPath;
                return true;
            }
            return false;
        }

        private void ExcludeFiletypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                FileFunc.IgnoredFiletypes.Add(Path.GetExtension(SourceFiles_listBox.SelectedItem.ToString()));
            }
        }

        private void SourceFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                var dupes = Dupes.GetDup(SourceFiles_listBox.SelectedIndex);

                if (dupes.DupFiles.Count == 0)
                {
                    SourceFile_Name_Label.Text = @"Name: = Na";
                    SourceFile_link.Text = @"Path: = Na";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                    SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                    return;
                }

                DuplicateFile_Num_Label.Text = $@"1/{dupes.DupFiles.Count}";
                string path1 = dupes.SourceFile.FullName;
                string path2 = dupes.DupFiles[0].FullName;
                if (File.Exists(path1) && File.Exists(path2))
                {
                    SourceFile_Name_Label.Text = $@"Name: = {Path.GetFileName(path1).Resize(40)}";

                    SourceFile_link.Text = $@"Path: = {path1.Resize(40)}";

                    DuplicateFile_Name_Label.Text = $@"Name: = {Path.GetFileName(path2).Resize(25)}";

                    DuplicateFile_link.Text = $@"Path: = {path2.Resize(40)}";
                }
                else
                {
                    SourceFile_Name_Label.Text = @"Name: = Na";
                    SourceFile_link.Text = @"Path: = Na";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                }
            }
        }

        private void SourceFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                Process.Start("explorer.exe", $"/select, { Dupes.GetDup(SourceFiles_listBox.SelectedIndex).SourceFile.FullName} ");
            }
        }

        private void DuplicateFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                Process.Start("explorer.exe", $"/select, { Dupes.GetDup(SourceFiles_listBox.SelectedIndex).DupFiles[Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]) -1].FullName}");
            }
        }

        private void Delete_SourceFile_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                var dupes = Dupes.GetDup(SourceFiles_listBox.SelectedIndex);
                if (dupes.SourceFile.Exists)
                {
                    if (!DeleteFile(dupes.SourceFile))
                        return;

                    SourceFiles_listBox.Items.Remove((SourceFiles_listBox.SelectedItem.ToString()));
                    Dupes.Remove(Dupes.GetDupint(dupes.SourceFile));

                    if (SourceFiles_listBox.Items.Count >= 1)
                    {
                        SourceFiles_listBox.SelectedIndex = 0;
                    }
                   else
                    {
                        SourceFile_Name_Label.Text = @"Name: = Na";
                        SourceFile_link.Text = @"Path: = Na";
                        DuplicateFile_Name_Label.Text = @"Name: = Na";
                        DuplicateFile_link.Text = @"Path: = Na";
                    }
                }
            }
        }

        private void Delete_DuplicateFile_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex == -1) return;

            var dupes = Dupes.GetDup(SourceFiles_listBox.SelectedIndex).DupFiles;
            var num = Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]) -1;
            if (num > dupes.Count) DuplicateFile_Num_Label.Text = @"1";
            if (dupes.Count <= 0)
            {
                SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                return;
            }

            if (dupes[num].Exists)
            {
                if (!DeleteFile(dupes[num]))
                    return;

                dupes.RemoveAt(num);

                if (dupes.Count >= 1)
                {
                    DuplicateFile_Num_Label.Text = @"1/1";
                    DuplicateFile_Name_Label.Text = $@"Name: = {dupes[0].Name.Resize(40)}";
                    DuplicateFile_link.Text = $@"Path: = {dupes[0].FullName.Resize(40)}";
                }
                else
                {
                    DuplicateFile_Num_Label.Text = @"1/1";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                }

                if (dupes.Count <= 0)
                {
                    SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                }
            }
        }

        private bool DeleteFile(FileInfo file)
        {
            try
            {
                file.Delete();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show($@"Access to the path {file} is denied");
            }
            catch (Exception)
            {
                //essageBox.Show(ex.Message);
            }
            return false;
        }

        private bool Openform(string formname)
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (!frm.Text.Equals(formname)) continue;

                frm.Show();
                Hide();
                return true;
            }

            return false;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (Scanbuton.Text == @"Stop Scan")
                return;

            if (!Openform("StartForm"))
            {
                var startForm = new StartForm();
                Hide();
                startForm.Show();
            }
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            if (Scanbuton.Text == @"Stop Scan")
                return;

            if (!Openform("Setings"))
            {
                var settings = new Settings();
                settings.Show();
                Hide();
            }
        }

        private void Delete_All_Dupes_Label_Click(object sender, EventArgs e)
        {
            return;

            if (SourceFiles_listBox.Items.Count == -1) return;

            var I = 0;
            var buffer2 = new List<int>();
            for (var index = 0; index < SourceFiles_listBox.Items.Count; index++)
            {
                var dup = Dupes.GetDup(I);
                var list = dup.DupFiles;
                var buffer = new List<FileInfo>();

                foreach (var s in list)
                {
                    if (s.Exists)
                    {
                        if (DeleteFile(s))
                        {
                            DuplicateFile_Num_Label.Text = @"1/1";
                            buffer.Add(s);
                        }
                    }
                }

                foreach (var s in buffer)
                {
                    list.Remove(s);
                }

                if (dup.DupFiles.Count <= 0)
                {
                    buffer2.Add(I);
                }

                I++;
            }

            if (buffer2.Count == -1)
                return;

            foreach(var k in buffer2.Reverse<int>())
            {
                SourceFiles_listBox.Items.RemoveAt(k);
                Dupes.Remove(k);
            }

            SourceFile_Name_Label.Text = @"Name: = Na";
            SourceFile_link.Text = @"Path: = Na";
            DuplicateFile_Name_Label.Text = @"Name: = Na";
            DuplicateFile_link.Text = @"Path: = Na";
        }

        private void Path_Box_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Path_Box.Text)
            {
                case ("Not Selected"):
                    {
                        return;
                    }
                case ("Music"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                        break;
                    }
                case ("Desktop"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                        break;
                    }
                case ("Videos"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                        break;
                    }
                case ("Document"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        break;
                    }
                case ("Custom Path"):
                    {
                        if (!SetupDirBool())
                        {

                        }
                        break;
                    }

                default:
                    _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    break;
            }
        }

        private void DuplicateFile_Left_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
                Change_File2_index(Directions.Left);
        }

        private void DuplicateFile_Right_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
                Change_File2_index(Directions.Right);
        }

        private enum Directions
        {
            Left = 0,
            Right = 1,
        }
        private void Change_File2_index(Directions direction)
        {
            var path2 = string.Empty;
            var go = false;
            int num = Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]);
            var dupes = Dupes.GetDup(SourceFiles_listBox.SelectedIndex);
            var list = dupes.DupFiles;

            switch (direction)
            {
                case Directions.Left:
                    if (--num >= 1)
                    {
                        path2 = list[num - 1].FullName;
                        go = true;
                        DuplicateFile_Num_Label.Text = $@"{num}/{list.Count}";
                    }

                    break;
                case Directions.Right:
                    if (++num <= dupes.DupFiles.Count)
                    {
                        path2 = list[num - 1].FullName;
                        go = true;
                        DuplicateFile_Num_Label.Text = $@"{num}/{list.Count}";
                    }

                    break;
            }

            if (File.Exists(path2) && go)
            {
                DuplicateFile_Name_Label.Text = $@"Name: = {Path.GetFileName(path2).Resize(40)}";
                DuplicateFile_link.Text = $@"Path: = {path2.Resize(40)}";
            }
        }

        private void UpdatTick_Tick(object sender, EventArgs e)
        {
            ScanprogressBar.Value = FileFunc.TotalProgress < 100 ? FileFunc.TotalProgress : 100;
            ProgresssLable.Text = $@"%{(FileFunc.TotalProgress < 100 ? FileFunc.TotalProgress : 100)}";
        }

        private void DuplicateFileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _fileHandler.FileHandled -= FileHandled;
            _fileHandler.FilesHandled -= FilesHandled;
        }
    }
}
