﻿namespace DuplicateFlleFinder
{
    partial class DuplicateFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SourceFiles_listBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.excludeFiletypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Scanbuton = new System.Windows.Forms.Button();
            this.Fileinfo_panel = new System.Windows.Forms.GroupBox();
            this.DuplicateFiles_GroupBox = new System.Windows.Forms.GroupBox();
            this.DuplicateFile_Num_Label = new System.Windows.Forms.Label();
            this.DuplicateFile_Left = new System.Windows.Forms.PictureBox();
            this.DuplicateFile_Right = new System.Windows.Forms.PictureBox();
            this.Delete_DuplicateFile = new System.Windows.Forms.Label();
            this.DuplicateFile_Name_Label = new System.Windows.Forms.Label();
            this.DuplicateFile_link = new System.Windows.Forms.LinkLabel();
            this.SourceFile_GroupBox = new System.Windows.Forms.GroupBox();
            this.Delete_SourceFile = new System.Windows.Forms.Label();
            this.SourceFile_Name_Label = new System.Windows.Forms.Label();
            this.SourceFile_link = new System.Windows.Forms.LinkLabel();
            this.Delete_All_Label = new System.Windows.Forms.Label();
            this.Path_Box = new System.Windows.Forms.ComboBox();
            this.ScanprogressBar = new System.Windows.Forms.ProgressBar();
            this.ProgresssLable = new System.Windows.Forms.Label();
            this.UpdatTick = new System.Windows.Forms.Timer(this.components);
            this.Close_Button = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1.SuspendLayout();
            this.Fileinfo_panel.SuspendLayout();
            this.DuplicateFiles_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DuplicateFile_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DuplicateFile_Right)).BeginInit();
            this.SourceFile_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Close_Button)).BeginInit();
            this.SuspendLayout();
            // 
            // SourceFiles_listBox
            // 
            this.SourceFiles_listBox.ContextMenuStrip = this.contextMenuStrip1;
            this.SourceFiles_listBox.FormattingEnabled = true;
            this.SourceFiles_listBox.Location = new System.Drawing.Point(8, 38);
            this.SourceFiles_listBox.Name = "SourceFiles_listBox";
            this.SourceFiles_listBox.Size = new System.Drawing.Size(288, 160);
            this.SourceFiles_listBox.TabIndex = 0;
            this.SourceFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.SourceFiles_listBox_SelectedIndexChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excludeFiletypeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.contextMenuStrip1.Size = new System.Drawing.Size(160, 26);
            // 
            // excludeFiletypeToolStripMenuItem
            // 
            this.excludeFiletypeToolStripMenuItem.Name = "excludeFiletypeToolStripMenuItem";
            this.excludeFiletypeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.excludeFiletypeToolStripMenuItem.Text = "Exclude Filetype";
            this.excludeFiletypeToolStripMenuItem.Click += new System.EventHandler(this.ExcludeFiletypeToolStripMenuItem_Click);
            // 
            // Scanbuton
            // 
            this.Scanbuton.Location = new System.Drawing.Point(8, 9);
            this.Scanbuton.Name = "Scanbuton";
            this.Scanbuton.Size = new System.Drawing.Size(140, 23);
            this.Scanbuton.TabIndex = 1;
            this.Scanbuton.Text = "Start Scan";
            this.Scanbuton.UseVisualStyleBackColor = true;
            this.Scanbuton.Click += new System.EventHandler(this.Scan_button_Click);
            // 
            // Fileinfo_panel
            // 
            this.Fileinfo_panel.Controls.Add(this.DuplicateFiles_GroupBox);
            this.Fileinfo_panel.Controls.Add(this.SourceFile_GroupBox);
            this.Fileinfo_panel.Location = new System.Drawing.Point(296, 12);
            this.Fileinfo_panel.Name = "Fileinfo_panel";
            this.Fileinfo_panel.Size = new System.Drawing.Size(288, 204);
            this.Fileinfo_panel.TabIndex = 10;
            this.Fileinfo_panel.TabStop = false;
            this.Fileinfo_panel.Text = "File Info";
            // 
            // DuplicateFiles_GroupBox
            // 
            this.DuplicateFiles_GroupBox.Controls.Add(this.DuplicateFile_Num_Label);
            this.DuplicateFiles_GroupBox.Controls.Add(this.DuplicateFile_Left);
            this.DuplicateFiles_GroupBox.Controls.Add(this.DuplicateFile_Right);
            this.DuplicateFiles_GroupBox.Controls.Add(this.Delete_DuplicateFile);
            this.DuplicateFiles_GroupBox.Controls.Add(this.DuplicateFile_Name_Label);
            this.DuplicateFiles_GroupBox.Controls.Add(this.DuplicateFile_link);
            this.DuplicateFiles_GroupBox.Location = new System.Drawing.Point(7, 110);
            this.DuplicateFiles_GroupBox.Name = "DuplicateFiles_GroupBox";
            this.DuplicateFiles_GroupBox.Size = new System.Drawing.Size(276, 86);
            this.DuplicateFiles_GroupBox.TabIndex = 3;
            this.DuplicateFiles_GroupBox.TabStop = false;
            this.DuplicateFiles_GroupBox.Text = "Duplicate Files";
            // 
            // DuplicateFile_Num_Label
            // 
            this.DuplicateFile_Num_Label.AutoSize = true;
            this.DuplicateFile_Num_Label.Location = new System.Drawing.Point(226, 15);
            this.DuplicateFile_Num_Label.Name = "DuplicateFile_Num_Label";
            this.DuplicateFile_Num_Label.Size = new System.Drawing.Size(24, 13);
            this.DuplicateFile_Num_Label.TabIndex = 13;
            this.DuplicateFile_Num_Label.Text = "1/1";
            // 
            // DuplicateFile_Left
            // 
            this.DuplicateFile_Left.Image = global::SimpleFileUtility.Properties.Resources.Left;
            this.DuplicateFile_Left.Location = new System.Drawing.Point(205, 10);
            this.DuplicateFile_Left.Name = "DuplicateFile_Left";
            this.DuplicateFile_Left.Size = new System.Drawing.Size(24, 22);
            this.DuplicateFile_Left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.DuplicateFile_Left.TabIndex = 12;
            this.DuplicateFile_Left.TabStop = false;
            this.DuplicateFile_Left.Click += new System.EventHandler(this.DuplicateFile_Left_Click);
            // 
            // DuplicateFile_Right
            // 
            this.DuplicateFile_Right.Image = global::SimpleFileUtility.Properties.Resources.Right;
            this.DuplicateFile_Right.Location = new System.Drawing.Point(246, 10);
            this.DuplicateFile_Right.Name = "DuplicateFile_Right";
            this.DuplicateFile_Right.Size = new System.Drawing.Size(24, 22);
            this.DuplicateFile_Right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.DuplicateFile_Right.TabIndex = 11;
            this.DuplicateFile_Right.TabStop = false;
            this.DuplicateFile_Right.Click += new System.EventHandler(this.DuplicateFile_Right_Click);
            // 
            // Delete_DuplicateFile
            // 
            this.Delete_DuplicateFile.AutoSize = true;
            this.Delete_DuplicateFile.ForeColor = System.Drawing.Color.Red;
            this.Delete_DuplicateFile.Location = new System.Drawing.Point(7, 59);
            this.Delete_DuplicateFile.Name = "Delete_DuplicateFile";
            this.Delete_DuplicateFile.Size = new System.Drawing.Size(57, 13);
            this.Delete_DuplicateFile.TabIndex = 10;
            this.Delete_DuplicateFile.Text = "Delete File";
            this.Delete_DuplicateFile.Click += new System.EventHandler(this.Delete_DuplicateFile_Click);
            // 
            // DuplicateFile_Name_Label
            // 
            this.DuplicateFile_Name_Label.AutoSize = true;
            this.DuplicateFile_Name_Label.Location = new System.Drawing.Point(6, 19);
            this.DuplicateFile_Name_Label.Name = "DuplicateFile_Name_Label";
            this.DuplicateFile_Name_Label.Size = new System.Drawing.Size(64, 13);
            this.DuplicateFile_Name_Label.TabIndex = 1;
            this.DuplicateFile_Name_Label.Text = "Name: = Na";
            // 
            // DuplicateFile_link
            // 
            this.DuplicateFile_link.AutoSize = true;
            this.DuplicateFile_link.Location = new System.Drawing.Point(6, 37);
            this.DuplicateFile_link.Name = "DuplicateFile_link";
            this.DuplicateFile_link.Size = new System.Drawing.Size(58, 13);
            this.DuplicateFile_link.TabIndex = 0;
            this.DuplicateFile_link.TabStop = true;
            this.DuplicateFile_link.Text = "Path: = Na";
            this.DuplicateFile_link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.DuplicateFile_LinkClicked);
            // 
            // SourceFile_GroupBox
            // 
            this.SourceFile_GroupBox.Controls.Add(this.Delete_SourceFile);
            this.SourceFile_GroupBox.Controls.Add(this.SourceFile_Name_Label);
            this.SourceFile_GroupBox.Controls.Add(this.SourceFile_link);
            this.SourceFile_GroupBox.Location = new System.Drawing.Point(6, 18);
            this.SourceFile_GroupBox.Name = "SourceFile_GroupBox";
            this.SourceFile_GroupBox.Size = new System.Drawing.Size(276, 86);
            this.SourceFile_GroupBox.TabIndex = 2;
            this.SourceFile_GroupBox.TabStop = false;
            this.SourceFile_GroupBox.Text = "Source File";
            // 
            // Delete_SourceFile
            // 
            this.Delete_SourceFile.AutoSize = true;
            this.Delete_SourceFile.ForeColor = System.Drawing.Color.Red;
            this.Delete_SourceFile.Location = new System.Drawing.Point(7, 59);
            this.Delete_SourceFile.Name = "Delete_SourceFile";
            this.Delete_SourceFile.Size = new System.Drawing.Size(57, 13);
            this.Delete_SourceFile.TabIndex = 9;
            this.Delete_SourceFile.Text = "Delete File";
            this.Delete_SourceFile.Click += new System.EventHandler(this.Delete_SourceFile_Click);
            // 
            // SourceFile_Name_Label
            // 
            this.SourceFile_Name_Label.AutoSize = true;
            this.SourceFile_Name_Label.Location = new System.Drawing.Point(6, 19);
            this.SourceFile_Name_Label.Name = "SourceFile_Name_Label";
            this.SourceFile_Name_Label.Size = new System.Drawing.Size(64, 13);
            this.SourceFile_Name_Label.TabIndex = 1;
            this.SourceFile_Name_Label.Text = "Name: = Na";
            // 
            // SourceFile_link
            // 
            this.SourceFile_link.AutoSize = true;
            this.SourceFile_link.Location = new System.Drawing.Point(6, 37);
            this.SourceFile_link.Name = "SourceFile_link";
            this.SourceFile_link.Size = new System.Drawing.Size(58, 13);
            this.SourceFile_link.TabIndex = 0;
            this.SourceFile_link.TabStop = true;
            this.SourceFile_link.Text = "Path: = Na";
            this.SourceFile_link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SourceFile_LinkClicked);
            // 
            // Delete_All_Label
            // 
            this.Delete_All_Label.AutoSize = true;
            this.Delete_All_Label.Enabled = false;
            this.Delete_All_Label.ForeColor = System.Drawing.Color.Red;
            this.Delete_All_Label.Location = new System.Drawing.Point(208, 201);
            this.Delete_All_Label.Name = "Delete_All_Label";
            this.Delete_All_Label.Size = new System.Drawing.Size(86, 13);
            this.Delete_All_Label.TabIndex = 10;
            this.Delete_All_Label.Text = "Delete All Dupes";
            this.Delete_All_Label.Click += new System.EventHandler(this.Delete_All_Dupes_Label_Click);
            // 
            // Path_Box
            // 
            this.Path_Box.FormattingEnabled = true;
            this.Path_Box.Items.AddRange(new object[] {
            "Not Selected",
            "Music",
            "Desktop",
            "Videos",
            "Document",
            "Custom Path"});
            this.Path_Box.Location = new System.Drawing.Point(154, 9);
            this.Path_Box.Name = "Path_Box";
            this.Path_Box.Size = new System.Drawing.Size(136, 21);
            this.Path_Box.TabIndex = 14;
            this.Path_Box.Text = "Not Selected";
            this.Path_Box.SelectedIndexChanged += new System.EventHandler(this.Path_Box_SelectedIndexChanged);
            // 
            // ScanprogressBar
            // 
            this.ScanprogressBar.Location = new System.Drawing.Point(38, 202);
            this.ScanprogressBar.Name = "ScanprogressBar";
            this.ScanprogressBar.Size = new System.Drawing.Size(169, 12);
            this.ScanprogressBar.TabIndex = 15;
            // 
            // ProgresssLable
            // 
            this.ProgresssLable.AutoSize = true;
            this.ProgresssLable.Location = new System.Drawing.Point(7, 201);
            this.ProgresssLable.Name = "ProgresssLable";
            this.ProgresssLable.Size = new System.Drawing.Size(21, 13);
            this.ProgresssLable.TabIndex = 16;
            this.ProgresssLable.Text = "%0";
            // 
            // UpdatTick
            // 
            this.UpdatTick.Interval = 1000;
            this.UpdatTick.Tick += new System.EventHandler(this.UpdatTick_Tick);
            // 
            // Close_Button
            // 
            this.Close_Button.Image = global::SimpleFileUtility.Properties.Resources.Close;
            this.Close_Button.Location = new System.Drawing.Point(559, 1);
            this.Close_Button.Name = "Close_Button";
            this.Close_Button.Size = new System.Drawing.Size(27, 23);
            this.Close_Button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Close_Button.TabIndex = 11;
            this.Close_Button.TabStop = false;
            this.Close_Button.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // DuplicateFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 220);
            this.Controls.Add(this.ProgresssLable);
            this.Controls.Add(this.ScanprogressBar);
            this.Controls.Add(this.Path_Box);
            this.Controls.Add(this.Delete_All_Label);
            this.Controls.Add(this.Close_Button);
            this.Controls.Add(this.Fileinfo_panel);
            this.Controls.Add(this.Scanbuton);
            this.Controls.Add(this.SourceFiles_listBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DuplicateFileForm";
            this.Text = "Duplicate_File_Form";
            this.contextMenuStrip1.ResumeLayout(false);
            this.Fileinfo_panel.ResumeLayout(false);
            this.DuplicateFiles_GroupBox.ResumeLayout(false);
            this.DuplicateFiles_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DuplicateFile_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DuplicateFile_Right)).EndInit();
            this.SourceFile_GroupBox.ResumeLayout(false);
            this.SourceFile_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Close_Button)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox SourceFiles_listBox;
        private System.Windows.Forms.Button Scanbuton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem excludeFiletypeToolStripMenuItem;
        private System.Windows.Forms.GroupBox Fileinfo_panel;
        private System.Windows.Forms.Label SourceFile_Name_Label;
        private System.Windows.Forms.LinkLabel SourceFile_link;
        private System.Windows.Forms.GroupBox SourceFile_GroupBox;
        private System.Windows.Forms.GroupBox DuplicateFiles_GroupBox;
        private System.Windows.Forms.Label DuplicateFile_Name_Label;
        private System.Windows.Forms.LinkLabel DuplicateFile_link;
        private System.Windows.Forms.Label Delete_SourceFile;
        private System.Windows.Forms.Label Delete_DuplicateFile;
        private System.Windows.Forms.PictureBox Close_Button;
        private System.Windows.Forms.Label Delete_All_Label;
        private System.Windows.Forms.ComboBox Path_Box;
        private System.Windows.Forms.ProgressBar ScanprogressBar;
        private System.Windows.Forms.Label ProgresssLable;
        private System.Windows.Forms.PictureBox DuplicateFile_Right;
        private System.Windows.Forms.PictureBox DuplicateFile_Left;
        private System.Windows.Forms.Label DuplicateFile_Num_Label;
        private System.Windows.Forms.Timer UpdatTick;
    }
}

