﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DuplicateFlleFinder
{
    public partial class DuplicateFileForm : Form
    {
        private string _scanDir;

        private readonly FileScaner _fileHandler = new FileScaner();

        public DuplicateFileForm()
        {
            InitializeComponent();
            _fileHandler.FilesHandled += FilesHandled;
            Path_Box.SelectedIndex = 2;

            _fileHandler.IgnoredFiletypes.AddRange(new string[] { ".lock", ".ide-shm" });
        }

        private void FilesHandled(object sender, bool Failed)
        {
            _fileHandler._ScanerProgress.StopScan = false;
            UpdatTick.Stop();

            if(!Failed)
            {
                foreach (var Dup in _fileHandler.DupList)
                {
                    SourceFiles_listBox.Items.Add($"{Dup.SourceFile.Name}");
                    //Console.WriteLine(Dup.SourceFile.Name ?? "test ");//?
                }

                Path_Box.Enabled = true;
                Fileinfo_panel.Enabled = true;
                Scanbuton.Text = @"Start Scan";
            }
            else
            {
                Path_Box.Enabled = true;
                Path_Box.Text = @"Folder access denied";
                Scanbuton.Text = @"Start Scan";
            }

            ScanprogressBar.Value = 100;
            ProgresssLable.Text = @"%100";

            if (SourceFiles_listBox.Items.Count == 0)
                MessageBox.Show("No Dupes Found..");
        }

        private void Scan_button_Click(object sender, EventArgs e)
        {
            if (Scanbuton.Text == @"Start Scan")
            {
                _fileHandler.InitializeScan(this, _scanDir);

                SourceFiles_listBox.Items.Clear();
                Fileinfo_panel.Enabled = false;
                Path_Box.Enabled = false;
                Scanbuton.Text = @"Stop Scan";
                UpdatTick.Start();
            }
            else
            {
                _fileHandler._ScanerProgress.StopScan = true;
            }
        }

        private bool SetupDirBool()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _scanDir = folderBrowserDialog.SelectedPath;
                return true;
            }
            return false;
        }

        private void ExcludeFiletypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                _fileHandler.IgnoredFiletypes.Add(Path.GetExtension(SourceFiles_listBox.SelectedItem.ToString()));
            }
        }

        private void SourceFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                var dupes = _fileHandler.DupList[SourceFiles_listBox.SelectedIndex];

                if (dupes.DupFiles.Count == 0)
                {
                    SourceFile_Name_Label.Text = @"Name: = Na";
                    SourceFile_link.Text = @"Path: = Na";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                    SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                    return;
                }

                DuplicateFile_Num_Label.Text = $@"1/{dupes.DupFiles.Count}";
                if (dupes.SourceFile.Exists && dupes.DupFiles[0].Exists)
                {
                    SourceFile_Name_Label.Text = $@"Name: = {dupes.SourceFile.Name.Resize(40)}";

                    SourceFile_link.Text = $@"Path: = {dupes.SourceFile.FullName.Resize(40)}";

                    DuplicateFile_Name_Label.Text = $@"Name: = {dupes.DupFiles[0].Name.Resize(25)}";

                    DuplicateFile_link.Text = $@"Path: = {dupes.DupFiles[0].FullName.Resize(40)}";
                }
                else
                {
                    SourceFile_Name_Label.Text = @"Name: = Na";
                    SourceFile_link.Text = @"Path: = Na";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                }
            }
        }

        private void SourceFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                Process.Start("explorer.exe", $"/select, { _fileHandler.DupList[SourceFiles_listBox.SelectedIndex].SourceFile.FullName} ");
            }
        }

        private void DuplicateFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                Process.Start("explorer.exe", $"/select, { _fileHandler.DupList[SourceFiles_listBox.SelectedIndex].DupFiles[Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]) -1].FullName}");
            }
        }

        private void Delete_SourceFile_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
            {
                var dup = _fileHandler.DupList[SourceFiles_listBox.SelectedIndex];
                if (dup.SourceFile.Exists)
                {
                    if (!dup.SourceFile.Delete(true))
                        return;

                    SourceFiles_listBox.Items.Remove(dup.SourceFile.Name);
                    _fileHandler.DupList.Remove(dup);

                    if (SourceFiles_listBox.Items.Count >= 1)
                    {
                        SourceFiles_listBox.SelectedIndex = 0;
                    }
                   else
                    {
                        SourceFile_Name_Label.Text = @"Name: = Na";
                        SourceFile_link.Text = @"Path: = Na";
                        DuplicateFile_Name_Label.Text = @"Name: = Na";
                        DuplicateFile_link.Text = @"Path: = Na";
                    }
                }
            }
        }

        private void Delete_DuplicateFile_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex == -1) return;

            var dupes = _fileHandler.DupList[SourceFiles_listBox.SelectedIndex].DupFiles;
            var num = Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]) -1;
            if (num > dupes.Count) DuplicateFile_Num_Label.Text = @"1";
            if (dupes.Count <= 0)
            {
                SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                return;
            }

            if (dupes[num].Exists)
            {
                if (!dupes[num].Delete(true))
                    return;

                dupes.RemoveAt(num);

                if (dupes.Count >= 1)
                {
                    DuplicateFile_Num_Label.Text = @"1/1";
                    DuplicateFile_Name_Label.Text = $@"Name: = {dupes[0].Name.Resize(40)}";
                    DuplicateFile_link.Text = $@"Path: = {dupes[0].FullName.Resize(40)}";
                }
                else
                {
                    DuplicateFile_Num_Label.Text = @"1/1";
                    DuplicateFile_Name_Label.Text = @"Name: = Na";
                    DuplicateFile_link.Text = @"Path: = Na";
                }

                if (dupes.Count <= 0)
                {
                    SourceFiles_listBox.Items.RemoveAt(SourceFiles_listBox.SelectedIndex);
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (Scanbuton.Text == @"Stop Scan")
                return;

            if (!this.OpenForm("StartForm"))
            {
                var startForm = new SimpleFileFinder.StartForm();
                Hide();
                startForm.Show();
            }
        }

        private void Delete_All_Dupes_Label_Click(object sender, EventArgs e)//reWork
        {
            if (SourceFiles_listBox.Items.Count == -1) return;

            var i = 0;
            var buffer2 = new List<int>();
            for (var index = 0; index < SourceFiles_listBox.Items.Count; index++)
            {
                var dup = _fileHandler.DupList[i];
                var list = dup.DupFiles;
                var buffer = new List<FileInfo>();

                foreach (var s in list)
                {
                    if (s.Exists)
                    {
                        if (s.Delete(true))
                        {
                            DuplicateFile_Num_Label.Text = @"1/1";
                            buffer.Add(s);
                        }
                    }
                }

                foreach (var s in buffer)
                {
                    list.Remove(s);
                }

                if (dup.DupFiles.Count <= 0)
                {
                    buffer2.Add(i);
                }

                i++;
            }

            if (buffer2.Count == -1)
                return;

            foreach(var k in buffer2.Reverse<int>())
            {
                SourceFiles_listBox.Items.RemoveAt(k);
                _fileHandler.DupList.RemoveAt(k);
            }

            SourceFile_Name_Label.Text = @"Name: = Na";
            SourceFile_link.Text = @"Path: = Na";
            DuplicateFile_Name_Label.Text = @"Name: = Na";
            DuplicateFile_link.Text = @"Path: = Na";
        }

        private void Path_Box_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Path_Box.Text)
            {
                case ("Not Selected"):
                    {
                        return;
                    }
                case ("Music"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                        break;
                    }
                case ("Desktop"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                        break;
                    }
                case ("Videos"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                        break;
                    }
                case ("Document"):
                    {
                        _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        break;
                    }
                case ("Custom Path"):
                    {
                        SetupDirBool();
                        break;
                    }

                default:
                    _scanDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    break;
            }
        }

        private void DuplicateFile_Left_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
                Change_File2_index(Directions.Left);
        }

        private void DuplicateFile_Right_Click(object sender, EventArgs e)
        {
            if (SourceFiles_listBox.SelectedIndex != -1)
                Change_File2_index(Directions.Right);
        }

        private enum Directions
        {
            Left = 0,
            Right = 1,
        }
        private void Change_File2_index(Directions direction)
        {
            var go = false;
            int num = Convert.ToInt16(DuplicateFile_Num_Label.Text.Split('/')[0]);
            var dupes = _fileHandler.DupList[SourceFiles_listBox.SelectedIndex];
            var list = dupes.DupFiles;

            switch (direction)
            {
                case Directions.Left:
                    if (--num >= 1)
                    {
                        go = true;
                        DuplicateFile_Num_Label.Text = $@"{num}/{list.Count}";
                    }

                    break;
                case Directions.Right:
                    if (++num <= dupes.DupFiles.Count)
                    {
                        go = true;
                        DuplicateFile_Num_Label.Text = $@"{num}/{list.Count}";
                    }

                    break;
            }

            if (list[num -1].Exists && go)
            {
                string temp = list[num - 1].FullName;
                DuplicateFile_Name_Label.Text = $@"Name: = {Path.GetFileName(temp).Resize(40)}";
                DuplicateFile_link.Text = $@"Path: = {temp.Resize(40)}";
            }
        }

        private void UpdatTick_Tick(object sender, EventArgs e)
        {
            var info = _fileHandler._ScanerProgress;

            ScanprogressBar.Value = info.TotalProgress < 100 ? info.TotalProgress : 100;
            ProgresssLable.Text = $@"%{(info.TotalProgress < 100 ? info.TotalProgress : 100)}";
        }

        private void DuplicateFileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _fileHandler.FilesHandled -= FilesHandled;
        }
    }
}
