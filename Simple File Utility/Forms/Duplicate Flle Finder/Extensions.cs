﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DuplicateFlleFinder
{
    public enum ContainsTypes { SourceFile = 1, DupFile = 2, None = 0 }

    public static class Extensions
    {
        private static int bufferSize = 1024;

        public static string Hash(this FileInfo file)
        {
            if (file.Length == 0)
                return "Na";

            return CheckMd5(file.FullName);
        }

        public static bool HasHash(this FileInfo file, FileInfo file2)
        {
            return file.Hash().Equals(file2.Hash());
        }

        private static string CheckMd5(string filename, bool buffer = false)
        {
            try
            {
                if (buffer)
                {
                    using (var md5 = MD5.Create())
                    {
                        using (var stream = new BufferedStream(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read), bufferSize))
                        {
                            return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty).ToLowerInvariant();
                        }
                    }
                }
                else
                {
                    using (var md5 = MD5.Create())
                    {
                        using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty).ToLowerInvariant();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return "Na";
            }
        }

        public static string Resize(this string text,int lenth)
        {
            if (text.Length < lenth)
                return text;

            return text.Substring(0, lenth);
        }

        public static List<FileInfo> Trim(this List<FileInfo> files, FileInfo item)
        {
            files.Remove(item);
            return files;
        }

        public static List<FileInfo> Remove(this List<FileInfo> list, List<FileInfo> fileInfos)
        {
            foreach (var file in fileInfos)
            {
                list.Remove(file);
            }

            return list;
        }
    }
}
