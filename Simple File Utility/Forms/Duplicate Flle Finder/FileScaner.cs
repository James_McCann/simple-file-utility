﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuplicateFlleFinder
{
    public class FileScaner
    {
        public List<string> IgnoredFiletypes { set; get; } = new List<string>();

        public ScanerProgress _ScanerProgress { private set; get; } = new ScanerProgress();

        public List<Dupes> DupList { private set; get; } = new List<Dupes>();

        public event EventHandler<bool> FilesHandled;

        private static void Invoke(Form form, Action action)
        {
            form.Invoke(action);
        }

        public void InitializeScan(Form uiform ,string scanDir)
        {
            Task.Factory.StartNew(() => 
            {
                DupList.Clear();
                DupList.AddRange(GetDupes(scanDir));

                Invoke(uiform, () => OnFilesHandled(DupList.Count > 0 ? false : true));
            });
        }

        protected virtual void OnFilesHandled(bool Failed) => FilesHandled?.Invoke(this, Failed);

        public List<Dupes> GetDupes(string scanDir)
        {
            List<FileInfo> Files = new List<FileInfo>();
            List<Dupes> dupeslist = new List<Dupes>();
            List<string> Foundfiles = new List<string>();

            try
            {
                Files.AddRange(new DirectoryInfo(scanDir).GetFiles("*.*", SearchOption.AllDirectories));
                Files.RemoveAll(x => IgnoredFiletypes.Contains(x.Extension));
                _ScanerProgress.TotalFilesToProcess = Files.Count;//TODO: fix progress bar
            }
            catch
            {
                Console.WriteLine(@"Failed to get files");
                return null;
            }

            foreach (var file in Files)
            {
                if (_ScanerProgress.StopScan)
                    break;

                if (Foundfiles.Contains(file.FullName))
                    continue;

                var files = Files.FindAll(x => x.Length == file.Length).Trim(file);
                var dupes = files.FindAll(x => file.HasHash(x));

                if (files.Count > 1 && dupes.Count > 0)
                {
                    Foundfiles.Add(file.FullName);
                    dupes.ForEach(x => Foundfiles.Add(x.FullName));

                    dupeslist.Add(new Dupes(file, dupes));
                    //Console.WriteLine($"Found: {file.Name}");
                }
            }

            return dupeslist;
        }
    }

    public class ScanerProgress
    {
        public bool StopScan { get; set; } = false;
        public int TotalProgress { set; get; } = 0;
        public double TotalFilesToProcess { set; get; } = 0;
        public double FilesProcessed { set; get; } = 0;
    }

    public class Dupes
    {
        public FileInfo SourceFile { private set; get; }

        public List<FileInfo> DupFiles { private set; get; }

        public Dupes(FileInfo sourceFile, List<FileInfo> dupFiles)
        {
            SourceFile = sourceFile;
            DupFiles = dupFiles;
        }
    }
}
