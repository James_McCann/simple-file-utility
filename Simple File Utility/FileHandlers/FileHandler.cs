﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Duplicate_File_Finder.FileHandlers
{
    public class FileEventArgs : EventArgs
    {
        public Dupes Dup { get; set; }
    }

    public class AllFilesProssedEventArgs : EventArgs
    {
        public bool Failed { get; set; }
    }

    public class FileHandler
    {
        private static void Invoke(Form form, Action action)
        {
            form.Invoke(action);
        }

        public event EventHandler<FileEventArgs> FileHandled;
        public event EventHandler <AllFilesProssedEventArgs>FilesHandled;

        public void InitializeScan(Form uiform ,string scanDir)
        {
            Task.Factory.StartNew(() => StartScan(scanDir,uiform));

            //Task<List<Dupes>> task = Task<List<Dupes>>.Factory.StartNew(() => FileFunc.GetPossableDupes(scanDir));
            //MessageBox.Show($"Done {task.Result.Count}");
        }

        private void StartScan(string scanDir, Form uiform)
        {
            Dupes.Clear();
            Hashes.HashList.Clear();
            var dList = FileFunc.GetDupes(scanDir);
            if (dList == null)
            {
                Invoke(uiform, () => OnFilesHandled(new AllFilesProssedEventArgs { Failed = true }));
                return;
            }

            foreach (var dup in dList)
            {
                Console.WriteLine($@"{dup.SourceFile}: Count = {dup.DupFiles.Count}");
                Dupes.StoreDup(dup);
                Invoke(uiform, () => OnFilehandled(new FileEventArgs { Dup = dup }));
            }

            Invoke(uiform, () => OnFilesHandled(new AllFilesProssedEventArgs { Failed = false }));

            if (!FileFunc.Soundplayer.IsAlive)
                FileFunc.Soundplayer.Start();
        }

        protected virtual void OnFilehandled(FileEventArgs args) => FileHandled?.Invoke(this, args);

        protected virtual void OnFilesHandled(AllFilesProssedEventArgs args) => FilesHandled?.Invoke(this, args);
    }
}
