﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Security.Cryptography;
using System.Threading;

namespace Duplicate_File_Finder.FileHandlers
{
    public class FileFunc
    {
        public static Thread Soundplayer = new Thread(PlaySound);
        public static List<string> IgnoredFiletypes = new List<string>();
        public static bool StopScan;
        public static int TotalProgress;
        public static double TotalFilesToProcess;
        public static double FilesProcessed;

        public bool IsDirEmpty(string path) => !Directory.EnumerateFileSystemEntries(path).Any();

        private static void PlaySound()
        {
            if (Properties.Settings.Default.CustomSoundFile && File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav"))
            {
                SoundPlayer player = new SoundPlayer($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav");
                player.Play();
                Thread.Sleep(Properties.Settings.Default.PlayTime);
                player.Stop();

            }
            else if (!Properties.Settings.Default.CustomSoundFile || !File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}\SoundFile.wav"))
            {
                SoundPlayer player = new SoundPlayer();
                player.Play();
                Thread.Sleep(Properties.Settings.Default.PlayTime);
                player.Stop();
            }
            Soundplayer = new Thread(PlaySound);
        }

        public static bool Comparefiles(FileInfo file1, FileInfo file2)
        {
            return file1.HasHash(file2);
        }

        public static List<FileInfo> Comparefiles(FileInfo file1, List<FileInfo> files)
        {
            return files.FindAll(x => file1.HasHash(x));
        }

        public static List<Dupes> GetDupes(string scanDir)
        {
            List<FileInfo> Files = new List<FileInfo>();
            List<Dupes> dupeslist = new List<Dupes>();
            List<string> Foundfiles = new List<string>();
            TotalProgress = 0;
            FilesProcessed = 0;
            TotalProgress = 0;

            try
            {
                Files.AddRange(new DirectoryInfo(scanDir).GetFiles("*.*", SearchOption.AllDirectories));
                TotalFilesToProcess = Files.Count;
            }
            catch
            {
                Console.WriteLine(@"Failed to get files");
                return null;
            }

            //Files = Files.FindAll(x => !IgnoredFiletypes.Contains(x.Extension));//temp fix

            foreach (var file in Files)
            {
                if (StopScan)
                    break;

                if (Foundfiles.Contains(file.FullName) || IgnoredFiletypes.Contains(file.Extension))
                    continue;

                var files = Files.FindAll(x => x.Length == file.Length && IgnoredFiletypes.Contains(x.Extension)).Trim(file);
                var dupes = Comparefiles(file, files);

                if (files.Count > 1 && dupes.Count > 0)
                {
                    Foundfiles.Add(file.FullName);
                    dupes.ForEach(x => Foundfiles.Add(x.FullName));

                    dupeslist.Add(new Dupes(file, dupes));
                    Console.WriteLine($"Found: {file.Name}");
                }
            }

            return dupeslist;
        }
    }
}
