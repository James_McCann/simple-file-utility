﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Duplicate_File_Finder.FileHandlers
{
    public class Dupes
    {
        #region Vars
        public enum ContainsTypes { SourceFile = 1, DupFile = 2, None = 3 }

        public static int Count() => AllDupList.Count;

        public FileInfo SourceFile { get; }

        public List<FileInfo> DupFiles;

        public static List<Dupes> AllDupList = new List<Dupes>();
        #endregion

        public Dupes(FileInfo sourceFile, List<FileInfo> dupFiles)
        {
            SourceFile = sourceFile;
            DupFiles = dupFiles;
        }

        public Dupes(FileInfo sourceFile, List<string> dupFiles)
        {
            SourceFile = sourceFile;
            DupFiles = new List<FileInfo>();

            dupFiles.ForEach(x => DupFiles.Add(new FileInfo(x)));
        }

        public Dupes(string sourceFile, List<string> dupFiles)
        {
            SourceFile = new FileInfo(sourceFile);
            DupFiles = new List<FileInfo>();

            dupFiles.ForEach(x => DupFiles.Add(new FileInfo(x)));
        }

        public static void StoreDup(Dupes dup, bool marge = false)
        {
            if (marge)
            {
                foreach (var temp in AllDupList)
                {
                    if (temp.SourceFile != dup.SourceFile) continue;

                    temp.DupFiles.AddRange(dup.DupFiles.Where(x => !temp.DupFiles.Contains(x)));
                    return;
                }
                AllDupList.Add(new Dupes(dup.SourceFile, dup.DupFiles));
            }
            else
            {
                foreach (Dupes temp in AllDupList)
                {
                    if (temp.SourceFile != dup.SourceFile) continue;

                    temp.DupFiles.Clear();
                    temp.DupFiles.AddRange(dup.DupFiles);
                    return;
                }
                AllDupList.Add(new Dupes(dup.SourceFile, dup.DupFiles));
            }
        }

        public static void Clear() => AllDupList.Clear();

        public static void Remove(int i) => AllDupList.RemoveAt(i);

        public static Dupes GetDup(int index) => index > AllDupList.Count ? throw new IndexOutOfRangeException() : AllDupList[index];

        public static int GetDupint(FileInfo File)
        {
            for (var i = 0; i < AllDupList.Count; i++)
            {
                if (AllDupList[i].SourceFile == File)
                    return i;
            }
            return -1;
        }

        public static Enum Contains(Dupes dup)
        {
            foreach (Dupes temp in AllDupList)
            {
                if (temp.SourceFile == dup.SourceFile)
                {
                    return ContainsTypes.SourceFile;
                }
                else
                {
                    if (dup.DupFiles.Any(t => temp.DupFiles.Contains(t)))
                    {
                        return ContainsTypes.DupFile;
                    }
                }
            }
            return ContainsTypes.None;
        }

        public static Enum ListContains(List<Dupes> dupList, FileInfo File, bool usebaselist = false)
        {
            var list = usebaselist ? AllDupList : dupList;
            foreach (Dupes Temp in list)
            {
                if (Temp.SourceFile == File)
                {
                    return ContainsTypes.SourceFile;
                }
                else
                {
                    for (int i = 0; i < Temp.DupFiles.Count; i++)
                    {
                        if (Temp.DupFiles.Contains(File))
                            return ContainsTypes.DupFile;
                    }
                }
            }
            return ContainsTypes.None;
        }
    }
}
