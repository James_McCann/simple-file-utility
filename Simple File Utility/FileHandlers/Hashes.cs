﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Duplicate_File_Finder.FileHandlers
{
    public class Hashes
    {
        #region Vars
        public static int Count() => HashList.Count;
        public string File { get; set; }
        public string Hash { get; set; }
        public static List<Hashes> HashList = new List<Hashes>();
        #endregion

        public Hashes(string file, string hash)
        {
            File = file;
            Hash = hash;
            HashList.Add(this);
        }

        public Hashes(FileInfo file)
        {
            File = file.FullName;
            Hash = file.Hash();
            HashList.Add(this);
        }

        public static bool Contains(string filepath) => HashList.Any(t => t.File == filepath);

        public static Hashes Gethash(int index) => index > HashList.Count ? throw new IndexOutOfRangeException() : HashList[index];

        public static int GetHashint(string filepath)
        {
            for (int i = 0; i < HashList.Count; i++)
            {
                if (HashList[i].File == filepath)
                    return i;
            }
            return -1; //item not in list
        }

        public static Hashes AddHash(string file, string hash) => new Hashes(file, hash);
    }
}
